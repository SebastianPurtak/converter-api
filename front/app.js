document.getElementById('button').onclick = () => {

    var formData = new FormData();
    var files = document.getElementById('fileHadler');
    var field1 = {
        type: "text",
        format: "doc",
        encoding: "utf8"
    };
    var field2 = {
        type: "text",
        format: "docx",
        encoding: "utf8"
    };
    var field3 = {
        type: "text",
        format: "pdf",
        encoding: "utf8"
    };

    field1 = JSON.stringify(field1);
    field2 = JSON.stringify(field2);
    field3 = JSON.stringify(field3);

    formData.append("test1", files.files[0]);
    formData.append("test2", files.files[1]);
    formData.append("test3", files.files[2]);
    formData.append("file1", field1);
    formData.append("file2", field2);
    formData.append("file3", field3);

    axios.post('https://converter-api.herokuapp.com/convertapi', formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        },
        responseType: "blob"
    })
        .then(function(response) {
            console.log('Response:', response);

            var contentType = Object.values(response.headers);

            console.log('Headers:', contentType);

            var blob = new Blob( [response.data], {type: contentType});
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = 'test.zip';
            link.click();
            
        })
        .catch(function(error) {
            console.log(error);
        });
};