const expect = require('expect');
const request = require('supertest');
const fs = require('fs');
const path = require('path');

const unzip = require('unzip2');

const {app} = require('./../server');
const {storageUploadedFiles, deleteFiles} = require('./../utils/utils');

var testFilesCopy = () => {
    // console.log('Copy function');
    var keepDir = __dirname + '/testsFiles/keep/';
    var testDir = __dirname + '/testsFiles/';
    var files = fs.readdirSync(keepDir);
    // console.log('files:', files); 
    for(var i = 0; i < files.length; i++){
        var oldFile = keepDir + files[i]; 
        var newFile = testDir + files[i];
        fs.copyFileSync(oldFile, newFile);
    };
};

var deleteResponse = () => {
    var resPath = path.join(__dirname, 'testsFiles/res');
    var files = fs.readdirSync(resPath);

    for(var i = 0; i < files.length; i++){
        if(files[i] !== '.gitkeep'){
            fs.unlinkSync(path.join(resPath, files[i]));
        };
    };
};

var binaryParser = (res, callback) => {
    res.setEncoding('binary');
    res.data = '';
    res.on('data', (chunk) => {
        res.data += chunk;
    });

    res.on('end', () => {
        callback(null, new Buffer(res.data, 'binary'));
    });
};

describe('UPLOAD AND DELETING FILES', () => {
    
    var files, uploadDirPath;
    
    before(() => {
        files = [{
            name: 'file1',
            path: __dirname + '/testsFiles/tekstTest1',
        }, {
            name: 'file2',
            path: __dirname + '/testsFiles/tekstTest2',  
        }, {
            name: 'file3',
            path: __dirname + '/testsFiles/tekstTest3',  
        }];
        uploadDirPath = __dirname + '/../upload/';
        testFilesCopy();
    });

    it('should pass environment test', () => {
        expect(true).toBe(true);
    });

    it('shoud pass server connection test', (done) => {
        request(app)
            .get('/convertapi')
            .expect(200)
            .expect((res) => {
                expect(res.text).toBe('Connection test')
            })
            .end(done);
    });

    it('should upload test files', (done) => {
        storageUploadedFiles(files);

        fs.readdir(uploadDirPath, (err, files) => {
            expect(files.length).toBe(4);
            expect(files[0]).toBe('.gitkeep');
            expect(files[1]).toBe('file1');
            expect(files[2]).toBe('file2');
            expect(files[3]).toBe('file3');
        });
        done();
    });

    it('should delete test files', () => {
        deleteFiles();
        var files = fs.readdirSync(uploadDirPath);
        expect(files.length).toBe(1);
        expect(files[0]).toBe('.gitkeep');
    });

});

describe('VALIDATION TESTS', () => {

    var filePath1, filePath2, filePath3, jpgPath, field, failPath; 

    before(() => {
        filePath1 = __dirname + '/testsFiles/tekstTest1';
        filePath2 = __dirname + '/testsFiles/tekstTest2';
        filePath3 = __dirname + '/testsFiles/tekstTest3';
        jpgPath = __dirname + '/testsFiles/vader-main.jpg';
        failPath = __dirname + '/testsFiles/failTest.js'
        field = JSON.stringify({type: "text", format: "pdf"});

        testFilesCopy();
    });

    it('should throw a no file uploaded error', (done) => {
        request(app)
            .post('/convertapi')
            .expect(400)
            .expect((res) => {
                expect(res.text).toBe('No file uploaded')
            })
            .end(done);
    });
    
    it('should throw a config field quantity error', (done) => {
        request(app)
            .post('/convertapi')
            .attach('test1', filePath1)
            .attach('test2', filePath2)
            .attach('test3', filePath3)
            .field('file1', field)
            .expect(400)
            .expect((res) => {
                expect(res.text).toBe('For each files must by a config field');
            })
            .end(done);        
    });

    it('should not upload a file with not accept type', (done) => {

        request(app)
            .post('/convertapi')
            .attach('test1', failPath)
            .field('file1', field)
            .expect(400)
            .expect((res) => {
                expect(res.text).toBe('Invalid file type');
            })
            .end(done);
    });

    it('should throw a config field type error', (done) => {
        field = JSON.stringify({type: "jpg", format: "pdf"});
        request(app)
            .post('/convertapi')
            .attach('test1', filePath1)
            .field('file1', field)
            .expect(400)
            .expect((res) => {
                expect(res.text).toBe('Invalid type field')
            })
            .end(done)  
    });

    it('should throw a config field text error', (done) => {
        var filePath1 = __dirname + '/testsFiles/tekstTest1';
        var field = JSON.stringify({type: "text", format: "jpg"});
        request(app)
            .post('/convertapi')
            .attach('test1', filePath1)
            .field('file1', field)
            .expect(400)
            .expect((res) => {
                expect(res.text).toBe('Invalid text field')
            })
            .end(done);  
    });

    it('should throw a config field graphic error', (done) => {
        var filePath1 = __dirname + '/testsFiles/testJPEG.jpeg';
        var field = JSON.stringify({type: "graph", format: "pdf"});
        request(app)
            .post('/convertapi')
            .attach('test1', filePath1)
            .field('file1', field)
            .expect(400)
            .expect((res) => {
                expect(res.text).toBe('Invalid text field')
            })
            .end(done);
    });
});

describe('TEXT CONVERTION TEST', function(){

    this.timeout(7000);

    var fileTxtPath, fileOdtPath, filePdfPath, fileDocPath, fileDocxPath, fieldTxt, fieldOdt, fieldPdf, fieldDoc, fieldDocx, form; 

    before(() => {
        testFilesCopy();

        fileTxtPath = __dirname + '/testsFiles/testTxt';
        fileOdtPath = __dirname + '/testsFiles/testOdt.odt';
        filePdfPath = __dirname + '/testsFiles/testPDF.pdf';
        fileDocxPath = __dirname + '/testsFiles/testWordDocx.docx';
        fileDocPath = __dirname + '/testsFiles/testWorDdoc.doc';

        fieldTxt = JSON.stringify({type: "text", format: "docx", encoding: "utf8"});
        fieldOdt = JSON.stringify({type: "text", format: "doc", encoding: "utf8"});
        fieldPdf = JSON.stringify({type: "text", format: "txt", encoding: "utf8"});
        fieldDoc = JSON.stringify({type: "text", format: "odt", encoding: "utf8"});
        fieldDocx = JSON.stringify({type: "text", format: "pdf", encoding: "utf8"});
    });

    it('should convert text files', (done) =>{
        request(app)
            .post('/convertapi')
            .attach('test1', fileTxtPath)
            .attach('test2', fileOdtPath)
            .attach('test3', filePdfPath)
            .attach('test4', fileDocxPath)
            .attach('test5', fileDocPath)
            .field('file1', fieldTxt)
            .field('file2', fieldOdt)
            .field('file3', fieldPdf)
            .field('file4', fieldDocx)
            .field('file5', fieldDoc)
            .expect(200)
            .buffer()
            .parse(binaryParser)
            .end((err, res) => {
                if(err){
                    console.log('err:', res.text);
                    return done(err);
                }else{

                    var wstream = fs.createWriteStream(path.join(__dirname, 'testsFiles/res/res.zip'));
                    wstream.write(res.body);
                    wstream.end();

                    var zipPath = path.join(__dirname, 'testsFiles/res/res.zip');
                    var unzipPath = path.join(__dirname, 'testsFiles/res');

                    var extract = fs.createReadStream(zipPath).pipe(unzip.Extract({path: unzipPath}));

                    extract.on('close', () => {
                        console.log("Extract end");

                        var resFiles = fs.readdirSync(path.join(__dirname, 'testsFiles/res'));
                        expect(resFiles).toContain('res.zip');
                        expect(resFiles).toContain('testTxt.docx');
                        expect(resFiles).toContain('testOdt.doc');
                        expect(resFiles).toContain('testPDF.txt');
                        expect(resFiles).toContain('testWordDocx.pdf');
                        expect(resFiles).toContain('testWorDdoc.odt');

                        deleteResponse();

                        done();
                    });
                }
            });
    });

});

describe('GRAPHIC CONVERTION TESTS', function(){
    this.timeout(7000);

    var fileJpegPath, filePngPath, fileBmpPath, fieldJpeg, fieldPng, fieldBmp;

    before(() => {
        fileJpegPath = __dirname + '/testsFiles/testJPEG.jpeg';
        filePngPath = __dirname + '/testsFiles/testPNG.png';
        fileBmpPath = __dirname + '/testsFiles/testBMP.BMP';

        fieldJpeg = JSON.stringify({type: "graph", format: "bmp"});
        fieldPng = JSON.stringify({type: "graph", format: "jpeg"});
        fieldBmp = JSON.stringify({type: "graph", format: "png"});
    });

    it('should convert graphic files', (done) => {
        request(app)
            .post('/convertapi')
            .attach('graph1', fileJpegPath)
            .attach('graph2', filePngPath)
            .attach('graph3', fileBmpPath)
            .field('file1', fieldJpeg)
            .field('file2', fieldPng)
            .field('file3', fieldBmp)
            .expect(200)
            .buffer()
            .parse(binaryParser)
            .end((err, res) => {
                if(err){
                    console.log('Error: ', res.text);
                    done(err);
                }else{
                    var wstream = fs.createWriteStream(path.join(__dirname, 'testsFiles/res/res.zip'));
                    wstream.write(res.body);
                    wstream.end();

                    var zipPath = path.join(__dirname, 'testsFiles/res/res.zip');
                    var unzipPath = path.join(__dirname, 'testsFiles/res'); 
                    
                    var extract = fs.createReadStream(zipPath).pipe(unzip.Extract({path: unzipPath}));

                    extract.on('close', () => {
                        console.log("Extract end");

                        var resFiles = fs.readdirSync(path.join(__dirname, 'testsFiles/res'));
                        expect(resFiles).toContain('res.zip');
                        expect(resFiles).toContain('testJPEG.bmp');
                        expect(resFiles).toContain('testPNG.jpeg');
                        expect(resFiles).toContain('testBMP.png');

                        deleteResponse();

                        done();
                    });
                };
            });
    });

});

describe('TABLE CONVERTION TESTS', function(){
    this.timeout(7000);

    var fileOdsPath, fileXlsPath, fileCsvPath, fieldOds, fieldXls, fieldCsv;

    before(() => {
        fileOdsPath = __dirname + '/testsFiles/testODS.ods';
        fileXlsPath = __dirname + '/testsFiles/testXLS.XLS';
        fileCsvPath = __dirname + '/testsFiles/testCSV.csv';

        fieldOds = JSON.stringify({type: "table", format: "xls"});
        fieldXls = JSON.stringify({type: "table", format: "csv"});
        fieldCsv = JSON.stringify({type: "table", format: "ods"});
    });

    it('should convert table files', (done) => {
        request(app)
            .post('/convertapi')
            .attach('table1', fileOdsPath)
            .attach('table2', fileXlsPath)
            .attach('table3', fileCsvPath)
            .field('file1', fieldOds)
            .field('file2', fieldXls)
            .field('file3', fieldCsv)
            .expect(200)
            .buffer()
            .parse(binaryParser)
            .end((err, res) => {
                if(err){
                    console.log('Error: ', res.text);
                    done(err);  
                }else{
                    var wstream = fs.createWriteStream(path.join(__dirname, 'testsFiles/res/res.zip'));
                    wstream.write(res.body);
                    wstream.end();

                    var zipPath = path.join(__dirname, 'testsFiles/res/res.zip');
                    var unzipPath = path.join(__dirname, 'testsFiles/res'); 
                    
                    var extract = fs.createReadStream(zipPath).pipe(unzip.Extract({path: unzipPath}));

                    extract.on('close', () => {
                        console.log("Extract end");

                        var resFiles = fs.readdirSync(path.join(__dirname, 'testsFiles/res'));
                        expect(resFiles).toContain('res.zip');
                        expect(resFiles).toContain('testODS.xls');
                        expect(resFiles).toContain('testXLS.csv');
                        expect(resFiles).toContain('testCSV.ods');

                        deleteResponse();

                        done();
                    });
                };
            });

    });
});
