const fs = require('fs');
const path = require('path');

const Joi = require('joi');
const _ = require('lodash');

const {deleteFiles} = require('./../utils/utils');

const typeSchema = Joi.string().valid('text', 'graph', 'table');

const textSchema = Joi.object().keys({
    type: Joi.string().valid('text').required(),
    format: Joi.string().valid('pdf', 'txt', 'doc', 'docx', 'odt').required(),
    encoding: Joi.string().valid('utf8', 'utf-8', 'ucs2', 'ucs-2', 'utf16le', 'utf-16le').required(),
});

const graphSchema = Joi.object().keys({
    type: Joi.string().valid('graph').required(),
    format: Joi.string().valid('jpg', 'jpeg', 'png', 'bmp').required(),
});

const tableSchema = Joi.object().keys({
    type: Joi.string().valid('table').required(),
    format: Joi.string().valid('csv', 'xls', 'xlsx', 'ods').required(),
});

const fileExtensionSchema = {
    type: Joi.array().items(Joi.string().valid(['', '.pdf', '.txt', '.doc', '.docx', '.odt', '.jpeg', '.jpg', '.png', '.bmp', '.BMP', '.ods', '.csv', '.xls', '.XLS', '.xlsx'])),
}; 

const fileExtensionTest = () => {
    var dirPath = __dirname + '/../upload/';
    var filesExtension = {
        type: []
    };
    var files = fs.readdirSync(dirPath);
    
    files.forEach((file) => {
        filesExtension.type.push(path.extname(file));
    });

    var result = Joi.validate(filesExtension, fileExtensionSchema);
    
    return result.error;
};

const fieldTypeTest = (fieldsArray) => {

    for(var i = 0; i < fieldsArray.length; i++){
        var result = Joi.validate(fieldsArray[i].type, typeSchema);
        if(result.error){
            return true;
        }
    };
    return false;
};

const fieldsValidationSwitch = (fieldsArray) => {
    for(var i = 0; i < fieldsArray.length; i++){
        if(fieldsArray[i].type === 'text'){
            var result = Joi.validate(fieldsArray[i], textSchema);
            if(result.error){
                return true;
            };
        }else if(fieldsArray[i].type === 'graph'){
            //console.log('funkcja walidujaca pola konfiguracji plików graficznych');
            var result = Joi.validate(fieldsArray[i], graphSchema);
            if(result.error){
                return true;
            };
        }else if(fieldsArray[i].type === 'table'){
            //console.log('funkcja walidujaca pola konfiguracji plików tabelarycznych');
            var result = Joi.validate(fieldsArray[i], tableSchema);
            if(result.error){
                return true;
            };
        }
    };
    return false;
}; 

const mainValidationTest = (fieldsArray, filesArray, res) => {
    var errorMessage;
    if(fieldsArray.length !== filesArray.length){
        errorMessage = 'For each files must by a config field';
        res.status(400).send(errorMessage);  
        deleteFiles();
        return true 
    }else if(fileExtensionTest()){
        errorMessage = 'Invalid file type';
        res.status(400).send(errorMessage);  
        deleteFiles();
        return true 
    }else if(fieldTypeTest(fieldsArray)){
        errorMessage = 'Invalid type field';
        res.status(400).send(errorMessage);  
        deleteFiles();
        return true 
    }else if(fieldsValidationSwitch(fieldsArray)){
        errorMessage = 'Invalid text field';
        res.status(400).send(errorMessage);  
        deleteFiles();
        return true 
    }else{
        return false;
    }
};

const initialValidation = (res, err, files) => {
    if(err){
        res.status(400).send(err);
        return true;
    }else if(_.isEmpty(files)){
        console.log('Files is empty!');
        res.status(400).send('No file uploaded');
        return true;
    }else{
        return false;
    }
};

module.exports = {
    initialValidation,
    mainValidationTest
};