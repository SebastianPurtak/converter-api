const path = require('path');
const fs = require('fs');

const Jimp = require('jimp');

const {fileHandler, newFilePatch} = require('./textConverter');

const graphConverter = (field, file, callback) => {
    var extension = path.extname(file.name);
    var filePath = fileHandler(file);
    var newPath = newFilePatch(file, field);

    Jimp.read(filePath, (err, image) => {
        if(err){
            console.log("Convertion error: ", err);
            throw err;
        };

        image.write(newPath, () => {
            callback();
        });

    });    

};

module.exports = {
    graphConverter,
};