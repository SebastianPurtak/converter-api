const path = require('path');
const fs = require('fs');

const textract = require('textract');
const officegen = require('officegen');
const pdf = require('pdfkit');
const WordExtractor = require('word-extractor');
const tika = require('tika');
const Jimp = require('jimp');
const xlsx = require('xlsx');
const pdfMake = require('pdfmake');
const {table} = require('table');

const {readFile, writeFile} = require('./tableConverter');

const newFilePatch = (file, field) => {
    var name = file.name.split('.');
    var newPath = __dirname + '/../converted/' + name[0] + '.' + field.format;
    return newPath;
};

const fileHandler = (file) => {
    var path = __dirname + '/../upload/' + file.name;
    return path; 
};

//PDF=Section===================================================================================================

const savePdf = (path, text, callback) => {
    var doc = new pdf;
    var fontPath = __dirname + '/fonts/Coda-Regular.ttf';

    doc.registerFont('Coda', fontPath);

    doc.pipe(fs.createWriteStream(path).on("finish", () => {
        callback();
    }));

    doc.font('Coda').text(text);

    doc.end();
};

const savePdfImage = (path, image, callback) => {
    var doc = new pdf;

    doc.pipe(fs.createWriteStream(path).on("finish", () => {
        callback();
    }));

    doc.image(image, {
        fit: [450, 500],
        align: 'center',
        valign: 'center'
    });

    doc.end();
};

const pdfTableGenerator = (tableTest) => {
    var tableWidth = [];
    for(var i = 0; i < tableTest[0].length; i++){
        tableWidth[i] = 'auto';
    };
    console.log('tableWidth: ', tableWidth);

    var tableDefinition = {
        content: [
            {
                table: {
                    headerRows: 1,
                    widths: tableWidth,
                    body: tableTest
                }
            }
        ]
    };

    return tableDefinition;
};

const savePdfTable = (path, table, callback) => {
    var fonts = {
        Roboto: {
            normal: __dirname + '/fonts/Roboto-Regular.ttf',
            bold: __dirname + '/fonts/Roboto-Medium.ttf',
            italics: __dirname + '/fonts/Roboto-Italic.ttf',
            bolditalics: __dirname + '/fonts/Roboto-Italic.ttf'
        }
    };

    var printer = new pdfMake(fonts);

    var docDefinition = pdfTableGenerator(table);

    var pdfDoc = printer.createPdfKitDocument(docDefinition);
    pdfDoc.pipe(fs.createWriteStream(path).on('close', () => {
        console.log('save pdf');
        callback();
    }));

    pdfDoc.end();
};

//PDF=Section===================================================================================================

//DOCX/DOC/ODT=Section==========================================================================================

const saveDocx = (text, newPath, file, callback) => {
    var docx = officegen('docx');
    
    var document = docx.createP();

    document.addText(text);

    var out = fs.createWriteStream(newPath);
    docx.generate(out);

    docx.on('error', (err) => {
        console.log(err);
    });
    out.on('close', () => {
        console.log('save docx');
        callback();
    });
};

const saveDocxImage = (image, newPath, file, callback) => {
    var docx = officegen('docx');
    
    var document = docx.createP();

    document.addImage(image);

    var out = fs.createWriteStream(newPath);
    docx.generate(out);

    docx.on('error', (err) => {
        console.log(err);
    });
    out.on('close', () => {
        console.log('save docx');
        callback();
    });
};

const tableContentGenerator = (data) => {
    var tableContent = [];

    for(var i = 0; i < data.length; i++){
        var row = [];
        for(var j = 0; j < data[i].length; j++){
            row[j] = {
                val: data[i][j],
                opts: {
                    shd: {
                        fill: "FFFFFF",
                    }
                }
            }
        }
        tableContent[i] = row;
    };

    return tableContent;
};

const saveDocxTable = (table, newPath, file, callback) => {
    var docx = officegen('docx');

    var tableContent = tableContentGenerator(table);

    var tableStyle = {
        tableColWidht: 4000,
        tableSize: 24,
        tableAlign: "left",
        tableFontFamily: "Comic Sans MS",
        borders: true,
    };

    docx.createTable(tableContent, tableStyle);

    var out = fs.createWriteStream(newPath);
    docx.generate(out);

    docx.on('error', (err) => {
        console.log(err);
    });
    out.on('close', () => {
        console.log('save docx');
        callback();
    });
};

//DOCX/DOC/ODT=Section==========================================================================================

//TXT/SAVE=STANDART=Section=====================================================================================

const saveTxtTable = (data, path, callback) => {
    var tableColumns = {};

    for(var i = 0; i < table.length; i++){
        tableColumns[i] = {
            alignment: 'center',
            minWidth: 10
        }
    };

    var config = {
        columns: tableColumns
    };

    var output = table(data, config);

    fs.writeFileSync(path, output);

    callback();
};

const saveStandard = (text, field, file) => {
    var encoding = field.encoding;
    console.log('Encoding: ', encoding);
    
    var newPath =  newFilePatch(file, field);
    fs.writeFileSync(newPath, text, encoding);
};

//TXT/SAVE=STANDART=Section=====================================================================================

//SAVE=SWITCH=Section===========================================================================================

const saveFile = (text, field, file, callback) => {
    var encoding = field.encoding;

    switch(field.format){
        case 'txt':
            console.log('save file as txt');
            saveStandard(text, field, file);
            callback();
            break;
        case 'odt':
            console.log('save file as odt');
            saveStandard(text, field, file);
            callback();
            break;
        case 'doc':
            console.log('save file as doc');
            var newPath =  newFilePatch(file, field);
            saveStandard(text, field, file);
            callback();
            break;
        case 'docx':
            console.log('save file as docx');
            newPath =  newFilePatch(file, field);
            saveDocx(text, newPath, file, () => {
                callback();
            });
            break;
        case 'pdf':
            console.log('save file as pdf');
            newPath =  newFilePatch(file, field);
            savePdf(newPath, text, () => {
                callback();
            });
            break;
        case 'jpg':
            console.log('save file as graphic');
            newPath =  newFilePatch(file, field);
            textToGraphic(text, newPath, () => {
                callback();
            });
            break;
    };
};

//SAVE=SWITCH=Section===========================================================================================

//READ=ODT/DOC/TXT=Section======================================================================================

const readOdt = (field, file, filePath, callback) => {
    var config = {
        preserveLineBreaks: true,
    };

    textract.fromFileWithPath(filePath, config, function (error, text) {
        if(error){
            console.log(error);
            return error;
        }else{   
                saveFile(text, field, file, () => {
                    callback();
                });
            };
        });
};

const readTxt = (field, file, filePath, callback) => {

    fs.readFile(filePath, 'utf8', (err, data) => {
        if(err){
            console.log(err);
            throw err;
        };

        saveFile(data, field, file, () => {
            callback();
        });
        
    });
};

const docRead = (field, file, filePath, callback) => {
    var extractor = new WordExtractor();
    var extracted = extractor.extract(filePath);

    extracted.then((doc) => {
        var data = doc.getBody();
        saveFile(data, field, file, () => {
            callback();
        });
    });
};

//READ=ODT/DOC/TXT=Section======================================================================================

//READ=PDF/DOCX=Section=========================================================================================

const tikaExtract = (data, options, callback) => {
    tika.text(data.filePath, options, (err, text) => {
        if(err){
            console.log(err);
            return err;
        }

        saveFile(text, data.field, data.file, () => {
            callback();
        });
    });
};

const docxRead = (field, file, filePath, callback) => {
    var data = {
        field,
        file,
        filePath,
    };

    var options = {
        contentType: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
    };

    tikaExtract(data, options, () => {
        callback();
    });
};

const pdfRead = (field, file, filePath, callback) => {
var data = {
        field,
        file,
        filePath
    };

    var options = {
        contentType: 'application/pdf'
    };

    tikaExtract(data, options, () => {
        callback();
    });  
};

//READ=PDF/DOCX=Section=========================================================================================

//READ=GRAPHIC=Section==========================================================================================

const pdfBmpGraphicParser = (filePath, image, newPath, callback) => {
    Jimp.read(filePath, (err, image) => {
        if(err){
            console.log("Convertion error: ", err);
            throw err;
        };

        var jpegPath = __dirname + '/../upload/test.jpeg';

        image.write(jpegPath, () => {
            console.log('newPath: ', newPath);
            savePdfImage(newPath, jpegPath, () => {
                callback();
            });
        });
    });
};

const readGraphic = (field, file, filePath, callback) => {
    var image = fs.readFileSync(filePath);
    var newPath =  newFilePatch(file, field);

    if(field.format === 'docx'){
        saveDocxImage(image, newPath, file, () => {
            callback();
        });
    }else if (field.format === 'pdf'){
        var extension = path.extname(file.name);

        if(extension === '.bmp' || extension === '.BMP'){
            pdfBmpGraphicParser(filePath, image, newPath, () => {
                callback();
            });
        }else{
            savePdfImage(newPath, image, () => {
                callback();
            });
        };
    }else{
        fs.writeFileSync(newPath, image);
        callback();
    };
};

//READ=GRAPHIC=Section==========================================================================================

//READ=TABLE=Section============================================================================================

const tableParser = (data) => {
    var table = data.split("\n");
    var newTable = [];
    for(var i = 0; i < (table.length - 1); i++){
        newTable[i] = table[i].split(","); 
    };
    return newTable;
};

const tableRead = (field, file, filePath, callback) => {
    var newPath =  newFilePatch(file, field);
    var data = readFile(filePath);
    var sheet = data.Sheets;
    var tableCsv = xlsx.utils.sheet_to_csv(sheet[data.SheetNames[0]]);

    var table = tableParser(tableCsv);

    if(field.format === 'pdf'){
        savePdfTable(newPath, table, () => {
            callback();
        });
    }else if(field.format === 'txt'){
        saveTxtTable(table, newPath, () => {
            callback();
        });
    }else{
        saveDocxTable(table, newPath, file, () => {
            callback();
        });
    };
};

//READ=TABLE=Section============================================================================================

//CONVERTER=SWITCH=Section======================================================================================

const textConverter = (field, file, callback) => {
    var extension = path.extname(file.name);
    var filePath = fileHandler(file);

    switch(extension){
        case '.odt':
            console.log('read odt');
            readOdt(field, file, filePath, () => {
                callback();
            });
            break;
        case '.doc':
            console.log('read doc');
            docRead(field, file, filePath, () => {
                callback();
            });
            break;
        case '.docx':
            console.log('read docx');
            docxRead(field, file, filePath, () => {
                callback();
            });
            break;
        case '.pdf':
            console.log('read pdf');
            pdfRead(field, file, filePath, () => {
                callback();
            });
            break;
        case '.BMP':
        case '.bmp':
        case '.png':
        case '.jpg':
        case '.jpeg':
            console.log('read graphic');
            readGraphic(field, file, filePath, () => {
                callback();
            });
            break;
        case '.csv':
        case '.xls':
        case '.xlsx':
        case '.ods':
            console.log('read ods');
            tableRead(field, file, filePath, () => {
                callback();
            });
        break;
        default:
            console.log('read txt');
            readTxt(field, file, filePath, () => {
                callback();
            });
            break;
    };
};

//CONVERTER=SWITCH=Section======================================================================================

module.exports = {
    textConverter,
    fileHandler, 
    newFilePatch,
    docRead
};