const fs = require('fs');
const path = require('path');

const archiver = require('archiver');
const formidable = require('formidable');

const {textConverter} = require('./textConverter');
const {graphConverter} = require('./graphConverter');
const {tableConverter} = require('./tableConverter');
const {app} = require('./../server');

const storageUploadedFiles = (filesArray) => {
    filesArray.forEach((file) => {
        var oldPath = file.path;
        var newPath = path.join(__dirname, '../upload', file.name);
        fs.renameSync(oldPath, newPath);
    });
};

const fieldsParse = (fields) => {
    var fieldsArray = Object.values(fields);

    fieldsArray = fieldsArray.map((field) => {
        return JSON.parse(field);
    });

    return fieldsArray;
};

const converterSwitch = (fieldsArray, filesArray, callback) => {
    for(var i = 0; i < filesArray.length; i++){
        if(fieldsArray[i].type === 'text'){
            //console.log('uruchamia funkcję kowertującą tekst')
            textConverter(fieldsArray[i], filesArray[i], () => {
                callback();
            });
        }else if(fieldsArray[i].type === 'graph'){
            //console.log('uruchamia funkcję kowertującą grafikę')
            graphConverter(fieldsArray[i], filesArray[i], () => {
                callback();
            });
        }else if(fieldsArray[i].type === 'table'){
            //console.log('uruchamia funkcję kowertującą tabele') 
            tableConverter(fieldsArray[i], filesArray[i], () => {
                callback();
            });
        }
    };
};

const zipModule = (res) => {
    var dirPath = path.join(__dirname, "../converted/");
    var zipDir = path.join(__dirname, '../test.zip');
    var files = fs.readdirSync(dirPath);
    var filePath = path.join(dirPath, files[1]);
    var fileMetaPath = path.join(dirPath, "fileZip.pdf");

    var archive = archiver('zip');

    for(var i = 0; i < files.length; i++){
        if(files[i] !== '.gitkeep'){
            archive.file(path.join(dirPath, files[i]), {name: files[i]});
        }
    };

    archive.pipe(res);

    archive.on('end', () => {
        res.end();
        console.log('res end');
        deleteFiles();
    });
    
    archive.finalize();
    
};

const deleteFiles = () => {
    var dirPath = path.join(__dirname, "../converted/");
    var files = fs.readdirSync(dirPath);

    for(var i = 0; i < files.length; i ++){
        if(files[i] !== '.gitkeep'){
            fs.unlinkSync(path.join(dirPath, files[i]));
        };
    };

    dirPath = path.join(__dirname, "../upload/");
    files = fs.readdirSync(dirPath);

    for(var i = 0; i < files.length; i ++){
        if(files[i] !== '.gitkeep'){
            fs.unlinkSync(path.join(dirPath, files[i]));
        };
    };
};

module.exports = {
    storageUploadedFiles,
    fieldsParse,
    converterSwitch,
    zipModule,
    deleteFiles
};