const path = require('path');
const fs = require('fs');

const {fileHandler, newFilePatch} = require('./textConverter');
const xlsx = require('xlsx');

const readFile = (filePath) => {
    var data = fs.readFileSync(filePath);
    
    data = xlsx.read(data, {type: 'buffer'});

    return data;
};

const writeFile = (data, newPath) => {
    var buff = xlsx.write(data, {type: 'buffer'});
    fs.writeFileSync(newPath, buff);

};

const csvWriter = (data, newPath) => {

    var buff = [];
    var sheet = data.Sheets;

    for(var i = 0; i < data.SheetNames.length; i++){
        buff.push(xlsx.utils.sheet_to_csv(sheet[data.SheetNames[i]]));
    };

    fs.writeFileSync(newPath, buff, "utf8");
};

const tableConverter = (field, file, callback) => {
    var extension = path.extname(file.name);
    var filePath = fileHandler(file);
    var newPath = newFilePatch(file, field);
    var data, csvData;

    data = readFile(filePath);

    if(field.format === 'csv'){
        csvWriter(data, newPath);
        callback();
    }else{
        writeFile(data, newPath);
        callback();
    };
};

module.exports = {
    tableConverter,
    readFile,
    writeFile
};