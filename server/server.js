const express = require('express');
const fs = require('fs');
const path = require('path');

const formidable = require('formidable');
const _ = require('lodash');

const {storageUploadedFiles, fieldsParse, converterSwitch, zipModule} = require('./utils/utils');
const {initialValidation, mainValidationTest} = require('./validation/validation');

const port = process.env.PORT || 3000;
const app = express();

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Content-Type', 'application/zip');
    res.header('Content-Disposition', 'attachment; filename=test.zip');
    res.attachment('archive-name.zip'); 
    next();
});

app.get('/convertapi', (req, res) => {
    res.status(200);
    res.send('Connection test');
    res.end();
});

app.post('/convertapi', (req, res) => {
    const form = new formidable.IncomingForm({
        uploadDir: path.join(__dirname, 'upload'),
        multiples: true,
        keepExtensions: true,
        maxFileSize: 200 * 1024 * 1024 
    });

    form.parse(req, (err, fields, files) => {

        if(initialValidation(res, err, files)){
            return res.end();
        };

        var fieldsArray = fieldsParse(fields);
        var filesArray = Object.values(files);
        storageUploadedFiles(filesArray);

        if(mainValidationTest(fieldsArray, filesArray, res)){
            return res.end();
        };

        var fileCounter = 0;                       
        converterSwitch(fieldsArray, filesArray, () => {
            fileCounter++;
            if(fileCounter === filesArray.length){
                zipModule(res);
            };
        });
    });
});

app.listen(port, () => {
    console.log(`Server started on port ${port}`);
});

module.exports = {app};